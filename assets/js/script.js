var selecteRow = null

function OnFormSubmit() {
    if(validate()) {
        var formData = GetFormData();
        if(selecteRow == null){
            InsertNewRecode(formData);
        }else{
            UpdateForm(formData);
        }
        ResetForm();
    }
}

function GetFormData() {
    var formData = {};
    formData["fullname"] = document.getElementById('fullname').value;
    formData["empcode"] = document.getElementById('empcode').value;
    formData["salary"] = document.getElementById('salary').value;
    formData["city"] = document.getElementById('city').value;
    return formData;
}


function InsertNewRecode(data) {
    var table = document.getElementById('employeelist').getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.fullname;

    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.empcode;

    cell3 = newRow.insertCell(2);
    cell3.innerHTML = data.salary;

    cell4 = newRow.insertCell(3);
    cell4.innerHTML = data.city;

    cell4 = newRow.insertCell(4);
    cell4.innerHTML = `<a onClick="OnEdit(this)" class="link" >Edit</a>
                        <a onClick ="OnDelete(this)" class="link">Delete</a>`;

}


function ResetForm(){
    document.getElementById("fullname").value = "";
    document.getElementById("empcode").value = "";
    document.getElementById("salary").value = "";
    document.getElementById("city").value = "";
    selecteRow = null;
}


function OnEdit(td){
    selecteRow = td.parentElement.parentElement;
    document.getElementById("fullname").value = selecteRow.cells[0].innerHTML;
    document.getElementById("empcode").value = selecteRow.cells[1].innerHTML;
    document.getElementById("salary").value = selecteRow.cells[2].innerHTML;
    document.getElementById("city").value = selecteRow.cells[3].innerHTML;
}


function UpdateForm(formData){
    selecteRow.cells[0].innerHTML = formData.fullname;
    selecteRow.cells[1].innerHTML = formData.empcode;
    selecteRow.cells[2].innerHTML = formData.salary;
    selecteRow.cells[3].innerHTML = formData.city;
}


function OnDelete(td){
    if(confirm("Are you sure delete this recode")) {
        row = td.parentElement.parentElement;
        document.getElementById("employeelist").deleteRow(row.rowIndex);
        ResetForm();
    }
}

function validate(){
    isValid = true;
    if( document.getElementById("fullname").value == ""){
        isValid = false;
        document.getElementById("fullnamevalidation").classList.remove('hide');
    } else {
        isValid = true;
        if (!document.getElementById("fullnamevalidation").classList.contains('hide')){
            document.getElementById("fullnamevalidation").classList.add('hide');
        }
    }
    return isValid;
}